/*Welcome. In this kata, you are asked to square every digit of
a number and concatenate them.
For example, if we run 9119 through the function, 811181 will come out, because 92 is 81 and 12 is 1. (81-1-1-81)Example #2: An input of 765 will/should return 493625 because 72 is 49, 62 is 36, and 52 is 25. (49-36-25)*/

function squareDigits(num){
    // Convert the number to a string to iterate through its digits
    let numStr = num.toString();
    
    // Use the map function to square each digit and join them as a string
    let result = numStr.split('').map(digit => parseInt(digit) ** 2).join('');
    
    return parseInt(result);
}
  
  // Example usage:
    let inputNumber = 9119;
    let outputNumber = squareDigits(inputNumber);
console.log(outputNumber);

/*A pangram is a sentence that contains every single letter of the alphabet at least once.
For example, the sentence "The quick brown fox jumps over the lazy dog" is a pangram, because it uses the letters A-Z at least once (case is irrelevant).
Given a string, detect whether or not it is a pangram.
Return True if it is, False if not. Ignore numbers and punctuation.*/

function isPangram(string) {
  // Convert the string to lowercase and remove non-alphabetic characters
  const cleanString = string.toLowerCase().replace(/[^a-z]/g, '');

  // Check if the set of unique characters in the clean string is equal to the set of all alphabets
  return new Set(cleanString).size === 26;
}

// Example usage:
const sentence = "The quick brown fox jumps over the lazy dog";
const result = isPangram(sentence);
console.log(result);  // Output: true

/*Take 2 strings s1 and s2 including only letters from a to z. Return a new sorted string, the longest possible, containing distinct letters - each taken only once - coming from s1 or s2.
Examples:
a = "xyaabbbccccdefww"
b = "xxxxyyyyabklmopq"
longest(a, b) -> "abcdefklmopqwxy"
a = "abcdefghijklmnopqrstuvwxyz"
longest(a, a) -> "abcdefghijklmnopqrstuvwxyz"*/

function longest(s1, s2) {
  // Combine the characters from both strings, remove duplicates, and sort
  const combinedString = Array.from(new Set(s1 + s2)).sort().join('');

  return combinedString;
}

// Examples:
const a = "xyaabbbccccdefww";
const b = "xxxxyyyyabklmopq";
console.log(longest(a, b));  // Output: "abcdefklmopqwxy"

const alphabet = "abcdefghijklmnopqrstuvwxyz";
console.log(longest(alphabet, alphabet));  // Output: "abcdefghijklmnopqrstuvwxyz"

/*Task
You will be given an array of numbers. You have to sort the odd numbers in ascending order while leaving the even numbers at their original positions.
Examples
[7, 1]  =>  [1, 7]
[5, 8, 6, 3, 4]  =>  [3, 8, 6, 5, 4]
[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]  =>  [1, 8, 3, 6, 5, 4, 7, 2, 9, 0]*/

function sortArray(array) {
  // Extract odd and even numbers into separate arrays
  const oddNumbers = array.filter(num => num % 2 !== 0);
  const evenNumbers = array.filter(num => num % 2 === 0);

  // Sort the odd numbers in ascending order
  const sortedOddNumbers = oddNumbers.sort((a, b) => a - b);

  // Merge the sorted odd numbers and even numbers back into the original positions
  const resultArray = array.map(num => (num % 2 !== 0 ? sortedOddNumbers.shift() : num));

  return resultArray;
}

// Examples:
console.log(sortArray([7, 1]));  // Output: [1, 7]
console.log(sortArray([5, 8, 6, 3, 4]));  // Output: [3, 8, 6, 5, 4]
console.log(sortArray([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]));  // Output: [1, 8, 3, 6, 5, 4, 7, 2, 9, 0]

/*Find the missing letter
Write a method that takes an array of consecutive (increasing) letters as input and that returns the missing letter in the array.
You will always get an valid array. And it will be always exactly one letter be missing. The length of the array will always be at least 2.
The array will always contain letters in only one case.
Example:
['a','b','c','d','f'] -> 'e'
['O','Q','R','S'] -> 'P'*/
function findMissingLetter(array) {
  for (let i = 0; i < array.length - 1; i++) {
    // Check if the ASCII difference between adjacent letters is more than 1
    if (array[i + 1].charCodeAt(0) - array[i].charCodeAt(0) > 1) {
      // Return the missing letter
      return String.fromCharCode(array[i].charCodeAt(0) + 1);
    }
  }

  // If no missing letter is found, return an empty string or another appropriate value
  return '';
}

// Examples:
console.log(findMissingLetter(['a', 'b', 'c', 'd', 'f']));  // Output: 'e'
console.log(findMissingLetter(['O', 'Q', 'R', 'S']));  // Output: 'P'


/*You are given an array (which will have a length of at least 3, but could be very large) containing integers.
The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N. Write a method that takes the array as an argument and returns this "outlier" N.
Examples:
[2, 4, 0, 100, 4, 11, 2602, 36] -->  11 (the only odd number)

[160, 3, 1719, 19, 11, 13, -21] --> 160 (the only even number)*/

function findOutlier(integers) {
  // Count the number of even and odd integers
  const evenCount = integers.filter(num => num % 2 === 0).length;
  const oddCount = integers.length - evenCount;

  // Determine if the majority is even or odd
  const isMajorityEven = evenCount > oddCount;

  // Find the outlier based on the majority
  for (let num of integers) {
    if ((isMajorityEven && num % 2 !== 0) || (!isMajorityEven && num % 2 === 0)) {
      return num;
    }
  }

  // Return 0 or another appropriate value if no outlier is found
  return 0;
}

// Examples:
console.log(findOutlier([2, 4, 0, 100, 4, 11, 2602, 36]));  // Output: 11
console.log(findOutlier([160, 3, 1719, 19, 11, 13, -21]));   // Output: 160

/*Your goal in this kata is to implement a difference function, which subtracts one list from another and returns the result.
It should remove all values from list a, which are present in list b keeping their order.
arrayDiff([1,2],[1]) == [2]
If a value is present in b, all of its occurrences must be removed from the other:
arrayDiff([1,2,2,2,3],[2]) == [1,3]*/

function arrayDiff(a, b) {
  // Use filter to keep elements in a that are not present in b
  return a.filter(element => !b.includes(element));
}

// Examples:
console.log(arrayDiff([1, 2], [1]));           // Output: [2]
console.log(arrayDiff([1, 2, 2, 2, 3], [2]));  // Output: [1, 3]

/*L'idée principale est de compter tous les caractères présents dans une chaîne. Si vous avez une chaîne comme aba, alors le résultat devrait être {'a': 2, 'b': 1}.
Et si la chaîne est vide ? Le résultat doit alors être un objet littéral vide, {}.*/

function count(string) {
  const charCount = {};

  for (let char of string) {
    charCount[char] = (charCount[char] || 0) + 1;
  }

  return charCount;
}

// Example:
const result = count("aba");
console.log(result);  // Output: { 'a': 2, 'b': 1 }


/*Write a program that will calculate the number of trailing zeros in a factorial of a given number.
N! = 1 * 2 * 3 *  ... * N

Be careful 1000! has 2568 digits...
Examples
zeros(6) = 1
# 6! = 1 * 2 * 3 * 4 * 5 * 6 = 720 --> 1 trailing zero

zeros(12) = 2
# 12! = 479001600 --> 2 trailing zeros*/

function zeros(n) {
  let count = 0;

  // Count the factors of 5 in the prime factorization of the factorial
  for (let i = 5; n / i >= 1; i *= 5) {
    count += Math.floor(n / i);
  }

  return count;
}

// Examples:
console.log(zeros(6));   // Output: 1
console.log(zeros(12));  // Output: 2


/*Complete the solution so that it splits the string into pairs of two characters.
If the string contains an odd number of characters then it should replace the missing second character of the final pair with an underscore ('_').
Examples:
* 'abc' =>  ['ab', 'c_']
* 'abcdef' => ['ab', 'cd', 'ef']*/

function solution(str) {
  const pairs = [];
  
  for (let i = 0; i < str.length; i += 2) {
      let pair = str.slice(i, i + 2);
      if (pair.length === 1) {
          pair += '_';
      }
      pairs.push(pair);
  }

  return pairs;
}

// Test cases
console.log(solution('abc'));      // Output: ['ab', 'c_']
console.log(solution('abcdef'));   // Output: ['ab', 'cd', 'ef']


/*Create a function taking a positive integer between 1 and 3999 (both included) as its parameter and returning a string containing the Roman Numeral representation of that integer.
Modern Roman numerals are written by expressing each digit separately starting with the left most digit and skipping any digit with a value of zero. In Roman numerals 1990 is rendered: 1000=M, 900=CM, 90=XC; resulting in MCMXC. 2008 is written as 2000=MM, 8=VIII; or MMVIII. 1666 uses each Roman symbol in descending order: MDCLXVI.
Example:
solution(1000); // should return 'M'
Help:
Symbol    Value
I          1
V          5
X          10
L          50
C          100
D          500
M          1,000*/


function solution(number) {
  if (number < 1 || number > 3999) {
      // Check if the number is within the valid range
      return 'Number out of range (1-3999)';
  }

  const romanNumerals = [
      { value: 1000, symbol: 'M' },
      { value: 900, symbol: 'CM' },
      { value: 500, symbol: 'D' },
      { value: 400, symbol: 'CD' },
      { value: 100, symbol: 'C' },
      { value: 90, symbol: 'XC' },
      { value: 50, symbol: 'L' },
      { value: 40, symbol: 'XL' },
      { value: 10, symbol: 'X' },
      { value: 9, symbol: 'IX' },
      { value: 5, symbol: 'V' },
      { value: 4, symbol: 'IV' },
      { value: 1, symbol: 'I' }
  ];

  let result = '';

  for (const numeral of romanNumerals) {
      while (number >= numeral.value) {
          result += numeral.symbol;
          number -= numeral.value;
      }
  }

  return result;
}

// Test case
console.log(solution(1000));  // Output: 'M'
console.log(solution(1990));  // Output: 'MCMXC'
console.log(solution(2008));  // Output: 'MMVIII'
console.log(solution(1666));  // Output: 'MDCLXVI'


/*Complete the solution so that the function will break up camel casing, using a space between words.

Example
"camelCasing"  =>  "camel Casing"
"identifier"   =>  "identifier"
""             =>  ""*/

function solution(string) {
  let result = '';

  for (let i = 0; i < string.length; i++) {
      // Check if the current character is uppercase
      if (string[i] === string[i].toUpperCase() && i > 0) {
          result += ' ';
      }

      result += string[i];
  }

  return result;
}

// Test cases
console.log(solution("camelCasing"));  // Output: "camel Casing"
console.log(solution("identifier"));   // Output: "identifier"
console.log(solution(""));              // Output: ""



/*We need to sum big numbers and we require your help.

Write a function that returns the sum of two numbers. The input numbers are strings and the function must return a string.

Example
add("123", "321"); -> "444"
add("11", "99");   -> "110"*/

function add(a, b) {
  let result = '';
  let carry = 0;

  // Make the lengths of both strings equal by adding leading zeros
  while (a.length < b.length) {
      a = '0' + a;
  }

  while (b.length < a.length) {
      b = '0' + b;
  }

  // Iterate through the digits from right to left
  for (let i = a.length - 1; i >= 0; i--) {
      const digitA = parseInt(a[i]);
      const digitB = parseInt(b[i]);

      const sum = digitA + digitB + carry;
      result = (sum % 10) + result;
      carry = Math.floor(sum / 10);
  }

  // If there is a carry after the loop, add it to the result
  if (carry > 0) {
      result = carry + result;
  }

  return result;
}

// Test cases
console.log(add("123", "321"));  // Output: "444"
console.log(add("11", "99"));    // Output: "110"



/*You need to write regex that will validate a password to make sure it meets the following criteria:

At least six characters long:
contains a lowercase letter
contains an uppercase letter
contains a digit
only contains alphanumeric characters (note that '_' is not alphanumeric)*/

// assign your RegExp to REGEXP:
const REGEXP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/;



/*Complete the solution so that it strips all text that follows any of a set of comment markers passed in. Any whitespace at the end of the line should also be stripped out.

Example:
Given an input string of:

apples, pears # and bananas
grapes
bananas !apples*/

function solution(input, markers) {
  // Create a regular expression pattern to match the markers
  const markerPattern = new RegExp(`[${markers.map(marker => `\\${marker}`).join('')}]`);

  // Split the input into lines
  const lines = input.split('\n');

  // Process each line to remove comments and trailing whitespace
  const result = lines.map(line => {
      const index = line.search(markerPattern);
      return index >= 0 ? line.slice(0, index).trim() : line.trim();
  });

  // Join the lines back together
  return result.join('\n');
}

// Example usage
const input = "apples, pears # and bananas\ngrapes\nbananas !apples";
const markers = ["#", "!"];

const output = solution(input, markers);
console.log(output);



/*In mathematics, the factorial of integer n is written as n!. It is equal to the product of n and every integer preceding it.
For example: 5! = 1 x 2 x 3 x 4 x 5 = 120
Your mission is simple: write a function that takes an integer n and returns the value of n!.
You are guaranteed an integer argument.
For any values outside the non-negative range, return null, nil or None (return an empty string "" in C and C++).
For non-negative numbers a full length number is expected for example, return 25! =  "15511210043330985984000000"  as a string.*/

function factorial(n) {
  // Check if n is a non-negative integer
  if (n < 0 || !Number.isInteger(n)) {
      return null; // Outside the non-negative range
  }

  // Base case: factorial of 0 is 1
  if (n === 0) {
      return '1';
  }

  // Recursive case: n! = n * (n-1)!
  const result = multiplyBigInt(n.toString(), factorial(n - 1));

  return result;
}

// Helper function to multiply two big integers represented as strings
function multiplyBigInt(a, b) {
  const result = Array(a.length + b.length).fill(0);

  for (let i = a.length - 1; i >= 0; i--) {
      for (let j = b.length - 1; j >= 0; j--) {
          const product = parseInt(a[i]) * parseInt(b[j]);
          const sum = product + result[i + j + 1];

          result[i + j] += Math.floor(sum / 10);
          result[i + j + 1] = sum % 10;
      }
  }

  // Remove leading zeros
  while (result[0] === 0) {
      result.shift();
  }

  return result.join('');
}

// Example usage
console.log(factorial(5)); // Output: "120"
console.log(factorial(25)); // Output: "15511210043330985984000000"
console.log(factorial(-1)); // Output: null


/*Create a function that takes a positive integer and returns the next bigger number that can be formed by rearranging its digits.
For example:
12 ==> 21
513 ==> 531
2017 ==> 2071

If the digits can't be rearranged to form a bigger number, return -1 (or nil in Swift, None in Rust):
9 ==> -1
111 ==> -1
531 ==> -1*/

function nextBigger(n) {
  const digits = n.toString().split('');
  let i = digits.length - 1;

  // Find the first pair of adjacent digits where digits[i] < digits[i+1]
  while (i > 0 && digits[i - 1] >= digits[i]) {
      i--;
  }

  // If no such pair is found, the number is already the largest possible
  if (i === 0) {
      return -1;
  }

  // Find the smallest digit to the right of digits[i-1] that is greater than digits[i-1]
  let j = digits.length - 1;
  while (digits[j] <= digits[i - 1]) {
      j--;
  }

  // Swap digits[i-1] with digits[j]
  [digits[i - 1], digits[j]] = [digits[j], digits[i - 1]];

  // Reverse the portion of the array to the right of digits[i-1]
  const reversed = digits.splice(i).reverse();
  digits.push(...reversed);

  // Convert the array back to a number
  const result = parseInt(digits.join(''));

  return result;
}

// Example usage
console.log(nextBigger(12));   // Output: 21
console.log(nextBigger(513));  // Output: 531
console.log(nextBigger(2017)); // Output: 2071
console.log(nextBigger(9));    // Output: -1
console.log(nextBigger(111));  // Output: -1
console.log(nextBigger(531));  // Output: -1
